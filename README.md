# logs-a-lot Overview

Simple python script that logs a bunch of messages to the console.  Use to test logging, etc.

## Deployment on OpenShift

To deploy run the following command:
`oc new-app registry.access.redhat.com/rhscl/python-36-rhel7~https://bitbucket.org/themoosman/logs-a-lot.git`

or 

To deploy run the following command:
`oc new-app registry.access.redhat.com/rhscl/python-36-rhel7~https://themoosman@bitbucket.org/themoosman/logs-a-lot.git`